<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masyarakat extends Model
{

    protected $table = 'masyarakat';


    use HasFactory;


    protected $fillable = [
        'NAMA',
        'NIK',
        'TANGGAL_LAHIR',
        'JENIS_KELAMIN',
        'PENDIDIKAN',
        'PENGELUARAN',
    ];
}
