<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masyarakat;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $year = ['1990','1991','1992','1993','1994','1995','1996','1997','1998','1999'];
        // $record = Masyarakat::selectRaw('YEAR(TANGGAL_LAHIR) as YEAR, PENDIDIKAN')
        //     ->groupBy('YEAR')->get();

        // $data = [];

        // foreach($record as $row) {
        //     $data['label'][] = $row->YEAR;
        //     $data['data'][] = (int) $row->PENDIDIKAN;
        // }

        // $data['chart_data'] = json_encode($data);

        return view('dashboard');
    }

}
