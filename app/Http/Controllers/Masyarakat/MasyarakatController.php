<?php

namespace App\Http\Controllers\Masyarakat;

use App\Http\Controllers\Controller;
use App\Models\Masyarakat;
use Illuminate\Http\Request;


class MasyarakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function table()
    {
        $masyarakat = Masyarakat::latest()->paginate(5);

        return view('masyarakat.table', compact('masyarakat'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masyarakat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'NIK' => 'required',  'NAMA' => 'required',  'PENGELUARAN' => 'required',
        ]);

        Masyarakat::create($request->all());

        return redirect()->route('masyarakat.table')
            ->with('success', 'Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function show(Masyarakat $Masyarakat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // mengambil data books berdasarkan id yang dipilih
        $masyarakat = Masyarakat::where('NO_ID', $id)->first();
        // passing data books yang didapat ke view edit.blade.php
        return view('masyarakat.edit', compact('masyarakat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'NIK' => 'required',  'NAMA' => 'required',  'PENGELUARAN' => 'required',
        ]);

        //fungsi eloquent untuk mengupdate data inputan kita
        Masyarakat::where('NO_ID', $id)->update($request->except(['_token']));

        //jika data berhasil diupdate, akan kembali ke halaman utama
        return redirect()->route('masyarakat.table')
            ->with('success', 'Masyarakat Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masyarakat $masyarakat)
    {
        $masyarakat->delete();

        return redirect()->route('masyarakat.table')
            ->with('success', 'Post deleted successfully');
    }
}
