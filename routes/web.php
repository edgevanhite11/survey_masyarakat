<?php

use App\Http\Controllers\{HomeController, DashboardController};
use App\Http\Controllers\Brg\BrgController;
use App\Http\Controllers\Masyarakat\MasyarakatController;

use Illuminate\Support\Facades\{Route, Auth};



//Route::view('/', 'welcome');

Auth::routes();

// Route::get('/', HomeController::class)->name('home');
Route::middleware('auth')->group(function () {
    Route::get('/', DashboardController::class)->name('dashboard');
    Route::get('dashboard', DashboardController::class)->name('dashboard');

    Route::prefix('masyarakat')->group(function () {

        Route::get('create', [MasyarakatController::class, 'create'])->name('masyarakat.create');
        Route::get('table', [MasyarakatController::class, 'table'])->name('masyarakat.table');
        Route::post('store', [MasyarakatController::class, 'store'])->name('masyarakat.store');


        Route::get('edit/{no_id}', [MasyarakatController::class, 'edit'])->name('masyarakat.edit');
        Route::post('update/{no_id}', [MasyarakatController::class, 'update'])->name('masyarakat.update');
    });
});
