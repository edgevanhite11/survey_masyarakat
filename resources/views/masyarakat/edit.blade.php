@extends('layouts.master')
@section('title','Masyarakat')
@push('custom-css')
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/summernote/summernote-bs4.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/jqvmap/jqvmap.min.css">

@endpush

@section('content')
<div class="container-fluid">

    <div class="alert alert-success" role="alert">
        <i class="fas fa-university"></i> Form Edit Masyarakat
    </div>



    <form id="frmbhn" action="{{ route('masyarakat.update',$masyarakat->NO_ID) }}" class="form-horizontal" method="post">
        @csrf
        @method('POST')
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>NIK</label>
                </div>
                <div class="col-md-2 ">
                    <input required type="text" id="NIK" name="NIK" value='{{ $masyarakat->NIK }}' class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>Nama</label>
                </div>
                <div class="col-md-4">
                    <input required type="text" id="NAMA" name="NAMA" value='{{ $masyarakat->NAMA }}' class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>Jenis Kelamin</label>
                </div>
                <div class="col-md-4">
                    <select class="form-control JENIS_KELAMIN" name="JENIS_KELAMIN" id="JENIS_KELAMIN" >
                        @if($masyarakat->JENIS_KELAMIN=='L')
                        <option value="L" selected>Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        @else
                        <option value="L">Laki-Laki</option>
                        <option value="P" selected>Perempuan</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>Tanggal Lahir</label>
                </div>
                <div class="col-md-3">
                    <input type="date" id="TANGGAL_LAHIR" name="TANGGAL_LAHIR" value='{{ $masyarakat->TANGGAL_LAHIR }}' class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>Pendidikan</label>
                </div>
                <div class="col-md-4">
                    <select class="form-control PENDIDIKAN" name="PENDIDIKAN" id="PENDIDIKAN" >
                        @if($masyarakat->PENDIDIKAN=='SD')
                        <option value="SD" selected>SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMA">SMA</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Sarjana">Sarjana</option>
                        @endif
                        @if($masyarakat->PENDIDIKAN=='SMP')
                        <option value="SD">SD</option>
                        <option value="SMP" selected>SMP</option>
                        <option value="SMA">SMA</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Sarjana">Sarjana</option>
                        @endif
                        @if($masyarakat->PENDIDIKAN=='SMA')
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMA" selected>SMA</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Sarjana">Sarjana</option>
                        @endif
                        @if($masyarakat->PENDIDIKAN=='Diploma')
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMA">SMA</option>
                        <option value="Diploma" selected>Diploma</option>
                        <option value="Sarjana">Sarjana</option>
                        @endif
                        @if($masyarakat->PENDIDIKAN=='Sarjana')
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMA">SMA</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Sarjana" selected>Sarjana</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-2">
                    <label>Pengeluaran</label>
                </div>
                <div class="col-md-2">
                    <input type="number" id="PENGELUARAN" name="PENGELUARAN" value='{{ $masyarakat->PENGELUARAN }}' class="form-control">
                </div>
            </div>
        </div>


        <button type="submit" class="btn btn-primary">Simpan</button>
        <a type="button" href="javascript:javascript:history.go(-1)" class="btn btn-danger">Cancel</a>

    </form>


</div><!-- /.container-fluid -->
@endsection

@push('custom-script')


@endpush
