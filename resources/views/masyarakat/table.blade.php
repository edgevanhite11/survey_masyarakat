@extends('layouts.master')
@section('title','Masyarakat')
@push('custom-css')
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/summernote/summernote-bs4.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="{{ asset('')}}assets/plugins/jqvmap/jqvmap.min.css">




@endpush

@section('content')

<div class="container-fluid">



    <div class="alert alert-success" role="alert">
        <i class="fas fa-university"></i> List Masyarakat
    </div>




    <div class="row">
        <div class="col-md-2">
            <a class="btn  btn-md btn-success" href="{{route('masyarakat.create')}}"> <i class="fas fa-plus fa-sm md-3"></i></a>
            <button type="submit" class="btn btn-md btn-danger" onclick="return confirm(' Are Your Sure? ')"> <i class="fas fa-trash fa-sm md-3"></i></button>
        </div>


        <!-- <div class="col-md-5">
            <form action="" method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search keyword.." name="keyword" value="" autocomplate="off" autofocus>
                    <div class="input-group-append">
                        <input class="btn btn-primary" type="submit" name="submit" value="search">
                    </div>
                </div>
            </form>
        </div> -->
    </div>


    <form method="post" action=" ">


        <table id="example" class="table table-bordered table-striped table-hover " style="width:100%; font-size: 13px">
            <thead>
                <tr>
                    <th width="20px"><input type="checkbox" id="selectall" /></th>
                    <th width="40px"></th>
                    <th id="NIK">NIK</th>
                    <th id="NAMA">Nama</th>
                    <th id="PENDIDIKAN">Pendidikan </th>
                    <th id="PENGELUARAN">Pengeluaran </th>
                </tr>
            </thead>

            <tbody>

                @foreach ($masyarakat as $masyarakatp )
                <tr>
                    <td width="20px"><input type='checkbox' class='singlechkbox' name='check[]' value=''></td>
                    <td width="40px">

                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">

                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('masyarakat.edit',$masyarakatp->NO_ID) }}">Edit</a>

                            </div>
                        </div>

                    </td>
                    <td>{{$masyarakatp->NIK}}</td>
                    <td>{{$masyarakatp->NAMA}}</td>
                    <td>{{$masyarakatp->PENDIDIKAN}}</td>
                    <td>{{$masyarakatp->PENGELUARAN}}</td>


                </tr>

                @endforeach

            </tbody>


        </table>

        {{$masyarakat->links()}}

    </form>


</div>

@endsection

@push('custom-script')

<!-- jQuery -->
<script src="{{ asset('')}}assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('')}}assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('')}}assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('')}}assets/dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('')}}assets/dist/js/demo.js"></script>
<script src="{{ asset('') }}assets/plugins/datatables/jquery.datatables.min.js"></script>
<script src="{{ asset('') }}assets/plugins/chart.js/Chart.min.js"></script>
<script src="{{ asset('') }}assets/plugins/raphael/raphael.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>

<script>
$('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf'
    ]
});
</script>

@endpush

